from django.shortcuts import render, redirect
from tasks.models import Task
from tasks.forms import TaskForm
from django.http import HttpRequest, HttpResponse
from django.contrib.auth.decorators import login_required


# Create your views here.
@login_required
def create_task(request: HttpRequest) -> HttpResponse:
    if request.method == "POST":
        form = TaskForm(request.POST)
        if form.is_valid():
            task = form.save()
            task.save()
            return redirect("list_projects")
    elif request.method == "GET":
        form = TaskForm()
    context = {"form": form}
    return render(request, "tasks/create_task.html", context)


@login_required
def show_my_tasks(request: HttpRequest) -> HttpResponse:
    tasks = Task.objects.filter(assignee=request.user)
    context = {"tasks": tasks}
    return render(request, "tasks/my_tasks.html", context)
