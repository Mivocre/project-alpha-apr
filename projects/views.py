from django.shortcuts import render, get_object_or_404, redirect
from projects.models import Project
from projects.forms import ProjectForm
from django.http import HttpRequest, HttpResponse
from django.contrib.auth.decorators import login_required

# Create your views here.


@login_required
def show_projects(request: HttpRequest) -> HttpResponse:
    projects = Project.objects.filter(owner=request.user)
    context = {"projects": projects}
    return render(request, "projects/list.html", context)


@login_required
def show_single_project(request: HttpRequest, id: int) -> HttpResponse:
    project = get_object_or_404(Project, id=id)
    context = {"project": project}
    return render(request, "projects/detail.html", context)


@login_required
def create_project(request: HttpRequest) -> HttpResponse:
    if request.method == "POST":
        form = ProjectForm(request.POST)
        if form.is_valid():
            project = form.save(False)
            project.save()
            return redirect("list_projects")
    elif request.method == "GET":
        form = ProjectForm()
    context = {"form": form}
    return render(request, "projects/create.html", context)
